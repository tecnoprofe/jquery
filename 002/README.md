# EJERCICIO

Realizar un script para controlar un elemento div con el metodo:
* .animate()
* .fadeOut()

### SINTAXIS .fadeIn()
```
    $(selector).fadeIn(speed,callback);
```
### SINTAXIS .fadeOut()
```
    $(selector).fadeOut(speed,callback);
```