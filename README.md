
# Archivos Jquery

Este repositorio tendrá varios ejemplos de códigos de jquery, usados para los estudiantes universitarios y publico en general. 

### Pre-requisitos 📋

_Debes de contar con lo siguiente:_

```
Conocimientos de programación básica
Conocimientos de HTML
Conocimientos de CSS

Ademas de contar con un editor como visual code (Recomendado)
```
## Licencia 📄

Este proyecto está bajo la Licencia MIT.

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Da las gracias públicamente 🤓.

---
⌨️ con mucha pasion por [TecnoProfe](https://gitlab.com/tecnoprofe)